# f = open('进程池演练.py')
# print(f.fileno())

# a = [1, 2, 3, 4]
# b = [a]*3
# b[1][3] = 9

# print(b)

# for i, j in enumerate(a):
#     print(i, j)

class A():
    num = 100

    def __init__(self):
        print(0000)
        self.a = []

    def demo(self):
        print("parent")
        print(hasattr(self, "a"))
        print(id(self.a))
        print(self.a)


class B(A):
    # def __init__(self):
    #     # super().__init__()
    #     print(123)
    #     pass

    def run(self):
        print(super().a)  # 报错

    def run1(self):
        print(self.num)
        print(super().num)
        self.a.append(111)
        self.demo()
        print(self.a)
        print(hasattr(self, "a"))
        print(id(self.a))

b = B()
b.run1()


