import time
import threading
from concurrent.futures import ThreadPoolExecutor

def _task():
    for i in range(2):
        print("执行工作, i={}. thread_id = {}.".format(i, threading.get_native_id()))
        time.sleep(1)
    return time.time()

tp = ThreadPoolExecutor(10)
'''
for i in range(10):
    # future: 返回future对象,保存任务的引用，等待任务完成，获取结果
    future = tp.submit(_task)
    print(future.result())   # future.result()会导致任务执行完才会出结果,才会下一个任务进入线程池，也就是串行了这种写法
'''
futures = []
for i in range(10):
    future = tp.submit(_task)
    futures.append(future)

for future in futures:
    print(future.result)
    