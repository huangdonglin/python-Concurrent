import os
import time


def urllist():
    """ 加载文件，并返回下载链接 """

    list_file = os.path.join("piclist/baidu.txt")
    with open(list_file, 'r') as f:
        url_list = [line.strip() for line in f]

    return url_list[:7]


class Timer():
    """ 计时器 """
    def __init__(self):
        self.val = 0

    def tick(self):
        """ 计时器开始 """
        self.val = time.time()

    def tock(self):
        """ 计时器结束 """
        return round(time.time() - self.val, 6)


