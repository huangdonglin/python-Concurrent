# hash模块和 下载模块结构基本一致，这里抽象出来就可以
from const import CalcType


class BaseModule():
    """ hash模块 """

    def __init__(self):
        self.calc_type = CalcType.singleThread

    def set_calc_type(self, type_):
        self.calc_type = type_

    def _process(self, url):
        pass

    def _process_singlethread(self, list_):
        """ 单线程处理函数 """
        pass

    def _process_multithread(self, list_):
        """ 多线程处理函数 """
        pass

    def _process_multiprocess(self, list_):
        """ 多进程处理函数 """
        pass

    def process(self, list_):
        """ 根据计算类型，调用不同的处理函数 """
        if self.calc_type == CalcType.singleThread:
            return self._process_singlethread(list_)
        elif self.calc_type == CalcType.MultiThread:
            return self._process_multithread(list_)
        elif self.calc_type == CalcType.MultiProcess:
            return self._process_multiprocess(list_)
        else:
            pass
