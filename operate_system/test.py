# 测试线程池
import time
import Task, pool


class SimpleTask(Task.Task):
    def __init__(self, callable):
        super().__init__(callable)

class AsyncTask(Task.AsyncTask):
    def __init__(self, callable):
        super().__init__(callable)
 
def process():
    time.sleep(1)
    print(123)
    time.sleep(1)
    print(456)


def test():
    # 1. 初始化线程池
    test_pool = pool.ThreadPool()
    test_pool.start()
    # 2. 生成一系列的任务
    for i in range(10):
        simple_task = SimpleTask(process)
        # 3. 往线程池，提交任务执行
        test_pool.put(simple_task)

    # test_pool.join()




# 异步测试1
def test_async():
    def async_process():
        num = 1
        for i in range(100):
            num += i
        time.sleep(1)
        return num


    # 1. 初始化线程池
    test_pool = pool.ThreadPool()
    test_pool.start()
    # 2. 生成一系列的任务
    for i in range(10):
        async_task = AsyncTask(async_process)
        # 3. 往线程池，提交任务执行
        test_pool.put(async_task)
        result = async_task.get_result()
        """ 执行这句话的时候，表明希望立马拿到函数的返回值。就会一直等待函数运行，实际上的线程池，也只是执行了一个任务 """
        # result_list.append(result)
        print(result)



# 异步测试2
def test_async():
    def async_process():
        num = 1
        for i in range(100):
            num += i
        time.sleep(1)
        return num


    # 1. 初始化线程池
    test_pool = pool.ThreadPool()
    test_pool.start()
    # 2. 生成一系列的任务 
    futures_list = list()
    for i in range(10):
        async_task = AsyncTask(async_process)
        # 3. 往线程池，提交任务执行
        test_pool.put(async_task)
        """ 这里主线程可以执行其他逻辑，而不用等 函数执行结果，而去处理其他事，就构成了异步
            futures_list 收集任务对象， 最后 处理完主线程逻辑， 再去一起输出 任务的结果。真正的实现并行，利用线程池    
        """
        futures_list.append(async_task)
        
        # result = async_task.get_result()
        # print(result)
    
    for task in futures_list:
        print(task.get_result())

if __name__ == '__main__':
    test_async()

