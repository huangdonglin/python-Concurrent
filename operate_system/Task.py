"""
    实现基本任务对象 Task， 保存用户任务处理逻辑
    1. 定义任务的参数
    2. 任务唯一标记（uuid）
    3. 任务执行逻辑
"""

import threading
import uuid

class Task:

    def __init__(self, func, *args, **kwargs):
        self.callable = func
        self.id = uuid.uuid4()

        self.args = args
        self.kwargs = kwargs
        
    
    def __str__(self):
        return str("Task id:  " + str(self.id))
    


""" 实现异步任务处理 AsyncTask
    ？不知道任务什么时候执行
    ？不知道任务什么时候执行完成

    解决： 通过条件变量
    给任务添加一个标记， 任务完成后，则标记为已完成
    任务完成时， 可直接获取任务运行结果
    任务未完成时， 获取任务结果， 会阻塞获取线程
"""
class AsyncTask(Task):

    def __init__(self, func, *args, **kwargs):
        super().__init__(func, *args, **kwargs)

        self.result = None
        self.condition = threading.Condition()
        

    def set_result(self, result):
        """ 设置运行结果 """
        with self.condition:
            self.result = result
            self.condition.notify()

    def get_result(self, ):
        """ 获取任务结果 """
        with self.condition:
            while not self.result:
                self.condition.wait()
                continue
            result = self.result
            return result
            


def my_task():
    print(123)

if __name__ == '__main__':
    task = Task(func = my_task)
    print(task)    




