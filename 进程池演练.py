import os 
import time
from concurrent.futures import ProcessPoolExecutor

pp = ProcessPoolExecutor(5)


def _task():
    for i in range(2):
        print("执行工作, i={}. process_id = {}.".format(i, os.getpid()))
        time.sleep(1)
    return time.time()


for i in range(10):
    pp.submit(_task)


