# 客户端
import socket

def client():
    """ 请求下载文件的客户端 """
    # 1. 创建套接字
    dw_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # 2. 连接服务器
    dest_ip = input("请输入下载服务器的ip地址：")
    dest_port =int(input("请输入端口："))
    dw_client.connect((dest_ip, dest_port))
    while True:
        # 3. 获取下载文件名
        dw_filename = input("请输入要下载的文件名字：")
        if dw_filename  == '123':
            break
        # 4. 发送文件名字到服务器
        dw_client.send(dw_filename.encode("utf-8"))
    print("关闭客户端")
    dw_client.close()

client()

