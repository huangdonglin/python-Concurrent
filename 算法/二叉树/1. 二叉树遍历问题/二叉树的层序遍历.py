# 二叉树的层序遍历： https://leetcode-cn.com/problems/binary-tree-level-order-traversal/submissions/


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


from collections import deque
class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        """
            使用栈解决： 如果结果只用一个列表返回，那么可以省略掉内层的for 循环
        """
        if not root: return []
        queue = deque()
        queue.append(root)
        rst = []
        while queue:
            n = len(queue)
            temp = []
            for i in range(n):
                node = queue.popleft()
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)
                temp.append(node.val)
            rst.append(temp)
        
        return rst


    # 还有一种，利用 显示栈的前序遍历， 元组标记当前节点 及 对应的层数
    # 还可以使用递归的方式， 都类似（递归的话，层的信息，当做一个函数参数记录， 递归时 deep + 1传递）
