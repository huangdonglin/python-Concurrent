# 题目以图片的形式，在本目录文件夹下

# 我的一些思路： 就是递归求解问题
class Node():
    pass

class Solution:

    def getSameSubNodeCount(self, root: Node) -> int:
        # 1.定义变量，累加计数（这里属于 闭包变量，也就是非全局变量）
        rst = 0

        def getNum(node):
            """ 求解左右子节点相同的个数 
                明白递归和回溯的过程， 可以在纸上画画理解过程，后序遍历，自底向上
            """
            # 2. 声明该变量不是局部变量
            nonlocal rst
            
            # 3. 递归终止边界条件： 当前节点为 None， 表示返回 0，按照题意
            if not node: return 0

            # 4. 递归求解 左右节点个数
            leftNum = getNum(node.left)
            rightNum = getNum(node.right)
            # 5. 比较是否相等
            if leftNum == rightNum:
                rst += 1
        
            # 6. 返回当前整棵树的节点个数， + 1 是算上此时的根节点。它们整个是上一级回溯的子节点个数
            return leftNum + rightNum + 1 
        
        getNum(root)

        return rst

# 执行
node = Node()
Solution().getSameSubNodeCount(node)

