# 左叶子之和： https://leetcode-cn.com/problems/sum-of-left-leaves/submissions/

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def sumOfLeftLeaves(self, root: TreeNode) -> int:
        """
            后序遍历，标记自己是左节点还是右节点， flag 1表示右，0表示左
        """
        if not root: return 0
        
        leftTotal = 0
        def inOrder(node, flag):
            nonlocal leftTotal
            if not node: 
                return 

            inOrder(node.left, 0)
            inOrder(node.right, 1)

            # 如果为左根节点， 值加入
            if not node.left and not node.right and flag == 0:
                leftTotal += node.val

        inOrder(root, 1)
        return leftTotal

# 前序遍历能解， 层序遍历还能解~ 就不再写了