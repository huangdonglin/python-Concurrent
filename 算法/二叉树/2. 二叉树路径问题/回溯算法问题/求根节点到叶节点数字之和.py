# 求根节点到叶节点数字之和: https://leetcode-cn.com/problems/sum-root-to-leaf-numbers/


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

class Solution:
    def sumNumbers(self, root: TreeNode) -> int:
        """ 和路径总和是一样 """
        rst = 0
        def dfs(node, total):
            nonlocal rst
            if not node: return 
            
            total = total * 10 + node.val
            if not node.left and not node.right:
                rst += total

            dfs(node.left, total)
            dfs(node.right, total)
        
        dfs(root, 0)
        return rst




