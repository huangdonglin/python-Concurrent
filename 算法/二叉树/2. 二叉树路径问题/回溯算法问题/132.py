class Solution:

    def a(self):
        num = 1
        def dfs():
            print(num)
            # 如果要更改 num（num += 1）， 就要声明为 非局部变量 nonlocal num， 
            # 否则作为不可变对象，修改等于重新创建新的对象（函数内的局部变量,）
            num += 1
            
        # a 里调用 dfs
        dfs()

Solution().a()

