# 力扣448号：https://leetcode-cn.com/problems/find-all-numbers-disappeared-in-an-array/


class Solution:
    def findDisappearedNumbers(self, nums):
        """ 个人写法，可查看官方写法，更优 """
        rst = list()

        # 循环遍历每一个数组元素
        for i in range(len(nums)):
            # 判断该元素位置上的值，是否需要交换， 注意 循环条件
            while nums[i] != i + 1 and nums[i] != nums[nums[i] - 1]:
                # 需要使用一个变量来代替，不能直接去交换
                a = nums[i] - 1 
                nums[i], nums[a] = nums[a], nums[i]
                
        # 再次遍历数组，找出缺失数字
        for i, num in enumerate(nums):
            if i + 1 != num:
                rst.append(i+1)
        return rst   

nums = [4,3,2,7,8,2,3,1]

s = Solution()
a = s.findDisappearedNumbers(nums)
print(a)
