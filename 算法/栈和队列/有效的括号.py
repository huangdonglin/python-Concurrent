# 有效的括号： https://leetcode-cn.com/problems/valid-parentheses/

class Solution:
    def isValid(self, s: str) -> bool:
        """
            使用栈进行求解，map记录一下
        """
        stack = []
        map  = {
            ")": "(",
            "]": "[",
            "}": "{"
        }
        for i in s:
            # 栈空的话，直接加进去
            if not stack: stack.append(i)
            # 如果为右边部分和栈尾进行判断
            elif i in map:
                if map[i] == stack[-1]:
                    stack.pop()
                else: 
                    return False
            # 否则为左边直接加进来
            else:
                stack.append(i)

        return not stack