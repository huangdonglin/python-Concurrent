# 简化路径： https://leetcode-cn.com/problems/simplify-path/

# 自己的想法，写出来，能对，效率还不低
class Solution:
    def simplifyPath(self, path: str) -> str:
        """
            定义一个栈， 依次扫描这个路径字符串,找出所有的文件名再使用 / 拼接，还需要一个临时字符串存储当前字符名
        """
        stack = list()
        i = 0
        while i < len(path):
            rst = ""
            # 找出文件名
            while i < len(path) and path[i] != '/':
                rst += path[i] 
                i += 1
            
            if rst == '.':
                pass
            elif rst == '':
                pass
            # 为 .. 表示上级目录， 执行 pop()
            elif rst == '..':
                if stack: stack.pop()
            else:
                # 正常文件名，加入到队列
                stack.append(rst)

            i += 1

        return '/' + '/'.join(stack)

