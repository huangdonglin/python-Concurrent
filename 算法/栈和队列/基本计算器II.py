# 基本计算器II：https://leetcode-cn.com/problems/basic-calculator-ii/

# 直接模拟，代码思路和基本计算器一样
class Solution:
    def calculate(self, s: str) -> int:
        """
            使用直接模拟的方式，代码类似于 基本计算器, 第一遍先计算乘除。再计算加减
            # 另外 Python 负数除法取整，不能使用 // ,结果不对，要使用 int(data.pop() / num)
        """
        preSign = 1
        rst = 0      
        i = 0
        n = len(s)
        num = 0
        data = list()
        for i in range(n):
            if s[i].isdigit():
                num = num*10 + ord(s[i]) - ord("0")
            elif s[i] == '-':
                if preSign == 2:
                    data.append(num * data.pop())
                elif preSign == 3:
                    data.append(int(data.pop() / num))
                else:
                    data.append(num * preSign)
                num = 0
                preSign = -1
            elif s[i] == '+':
                if preSign == 2:
                    data.append(num * data.pop())
                elif preSign == 3:
                    data.append(int(data.pop() / num))
                else:
                    data.append(num * preSign)
                num = 0
                preSign = 1
            elif s[i] == '*':
                if preSign == 2:
                    data.append(num * data.pop())
                elif preSign == 3:
                    data.append(int(data.pop() / num))
                else:
                    data.append(num * preSign)
                num = 0
                preSign = 2
            elif s[i] == '/':
                if preSign == 2:
                    data.append(num * data.pop())
                elif preSign == 3:
                    data.append(int(data.pop() / num))
                else:
                    data.append(num * preSign)
                num = 0
                preSign = 3
        
        # 最后一个符号还可能是 乘除，要进行操作
        if preSign == 2:
            data.append(num * data.pop())
        elif preSign == 3:
            data.append(int(data.pop() / num))
        else:
            data.append(num * preSign)

        return sum(data)

# 优化if代码
class Solution:
    def calculate(self, s: str) -> int:
        preSign = 1
        n, num = len(s), 0
        # 数据栈
        data = list()
        # 判断preSign取什么符号
        Map = {'+': 1, '-': -1, '*': 2, '/': 3}
        for i in range(n):
            # 求数字
            if s[i].isdigit():
                num = num*10 + ord(s[i]) - ord("0")
            # + - * / 判断下处理
            if  i == n-1 or s[i] in '+-*/':
                # 判断当前获得的num 它的符号是啥，如果是乘除的话，需要拿到上一个数相乘除
                if preSign == 2:
                    data.append(num * data.pop())
                elif preSign == 3:
                    data.append(int(data.pop() / num))
                # 否则加减法直接当做 num的正负号，把num 压入栈里
                else:
                    data.append(num * preSign)
                num = 0
                # 根据当前的符号，确定下一个数字的符号是什么
                preSign = Map.get(s[i])

        return sum(data)


d = "3+2*2"

s = Solution()
rst = s.calculate(d)
print(rst)


