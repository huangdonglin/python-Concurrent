# 按奇偶排序数组II： https://leetcode-cn.com/problems/sort-array-by-parity-ii/

class Solution:
    def sortArrayByParityII(self, nums: list) -> list:
        i, j, n = 0, 1, len(nums)

        while i < n:
            if nums[i] % 2 != 0:
                nums[i], nums[j] = nums[j], nums[i]
                j += 2
            else: 
                i += 2

        return nums

