# 合并数组：https://leetcode-cn.com/problems/merge-intervals/

# 官方解法: 这道题难在就一种思路，换一种思路的话，就不好解，比方说我想了很久，现在原始数组里面找到一个合并区间放进去，在再原始数组里寻找，直到找完。。
class Solution:
    def merge(self, intervals: list) -> list:
        intervals.sort(key=lambda x: x[0])

        merged = []
        for interval in intervals:
            # 如果列表为空，或者当前区间与上一区间不重合，直接添加
            if not merged or merged[-1][1] < interval[0]:
                merged.append(interval)
            else:
                # 否则的话，我们就可以与上一区间进行合并
                merged[-1][1] = max(merged[-1][1], interval[1])

        return merged



intervals = [[1,3],[2,6],[8,10],[15,18]]

s = Solution()
rst = s.merge(intervals)
print(rst)



