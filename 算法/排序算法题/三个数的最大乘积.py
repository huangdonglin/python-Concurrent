# 三个数得乘积: https://leetcode-cn.com/problems/maximum-product-of-three-numbers/

# 不使用排序，直接记录出 最大三个值，和最小两个值
class Solution:
    def maximumProduct(self, nums: list) -> int:
        min1 = min2 = 10000
        max1 = max2 = max3 = -10000

        for num in nums:
            if num > max1:
                max3 = max2
                max2 = max1
                max1 = num
            elif num > max2:
                max3 = max2
                max2 = num
            elif num > max3:
                max3 = num
            
            if num < min1:
                min2 = min1
                min1 = num
            elif num < min2:
                min2 = num

        rst1, rst2 = min1*min2*max1, max1*max2*max3

        return rst1 if rst1 > rst2 else rst2


