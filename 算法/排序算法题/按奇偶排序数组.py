# 按奇偶排序数组： 


class Solution:
    def sortArrayByParity(self, nums: list) -> list:
        """
            利用快排的二向切分逻辑
        """
        fast, slow, n = 0, 0, len(nums)
        while fast < n:
            if nums[fast] % 2 == 0:
                nums[fast], nums[slow] = nums[slow], nums[fast]
                fast += 1; slow += 1
            else:
                fast += 1

        return nums


class Solution:
    def sortArrayByParity(self, nums: list) -> list:
        """
            利用三路排序分区（最后还是只剩俩区）
        """
        n = len(nums)
        less, great, i = 0, n-1, 0
        while i < great:
            if nums[i]%2 == 0:
                nums[less], nums[i] = nums[i], nums[less]
                i += 1; less += 1
            else:
                nums[great], nums[i] = nums[i], nums[great]
                great -= 1
        
        return nums