# 1. 首先接收所有的输入
data = []
while True:
    try:
        arr = input().split(",")
        for i in range(len(arr)):
            arr[i] = int(arr[i])
        data.append(arr)
    except:
        break

# data = [[1, 2, 4, 6, 9], [2, 4, 5, 6, 8], [3, 5, 6, 20, 34]]

# 2. 找到所有的共同元素：
arr1 = data[0]
# 迭代去找！
for i in range(1, len(data)):
    arr2 = data[i]
    k = 0
    j = 0
    # 记录相同值的列表
    sameList = []
    # O(n) 比较出相同元素
    while k < len(arr1) and j < len(arr2):
        if arr2[j] < arr1[k]:
            j += 1
        elif arr2[j] > arr1[k]:
            k += 1
        else:
            sameList.append(arr2[j])
            j += 1

    # 然后继续寻找
    arr1 = sameList

# 最后 arr2 就是共同的部分, 返回最小的结果
print(arr1[0])
