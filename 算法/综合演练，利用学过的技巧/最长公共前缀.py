# 最长公共前缀： https://leetcode-cn.com/problems/longest-common-prefix/submissions/

# 能通过
class Solution:
    def longestCommonPrefix(self, strs: list) -> str:
        """
            暴力解法， 取第一个字符串， 一个一个字符尝试和后面的比较（纵向）
        """
        rst = ''
        for i in range(len(strs[0])):
            c = strs[0][i]
            for j in range(len(strs)):
                if i >= len(strs[j]) or strs[j][i] != c:
                    return rst

            rst += c

        return rst

# 横向，纵向， 二分， 分治法。 很多， 可以了解一下，比较磨炼思维

