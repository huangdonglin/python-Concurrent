# 寻找旋转排序数组中的最小值: https://leetcode-cn.com/submissions/detail/194625990/

class Solution:
    def findMin(self, nums: list) -> int:
        left, right = 0, len(nums) - 1

        while left < right:
            mid = left + (right - left) // 2
            # 最小值在mid前面
            if nums[mid] > nums[right]:
                left = mid + 1
            else:
                right = mid

        return nums[left]

nums = [1,3,5]

s = Solution()
rst = s.findMin(nums)
print(rst)
