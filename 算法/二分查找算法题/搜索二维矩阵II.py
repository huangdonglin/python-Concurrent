# 搜索二维矩阵II: https://leetcode-cn.com/problems/search-a-2d-matrix-ii/

# 从左下角遍历
class Solution:
    def searchMatrix(self, matrix: list, target: int) -> bool:
        row, col = len(matrix) - 1, len(matrix[0])
        i, j = row, 0
        while i >= 0 and j < col:
            if matrix[i][j] == target:
                return True
            elif matrix[i][j] < target:
                j = j + 1
            else:
                i = i - 1

        return False


# 还有二分查找的方法，感觉效率也并不高呀~ 不记录了，没尝试
