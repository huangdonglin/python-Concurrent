# 寻找峰值： https://leetcode-cn.com/problems/find-peak-element/
# 代码看似很常规简单，实则不容易~ 建议观看官方题解，我想不到

class Solution:
    def findPeakElement(self, nums: list) -> int:
        left, right = 0, len(nums) - 1
        while left < right:
            mid = left + (right - left) // 2
            if nums[mid] > nums[mid+1]: # 有下降
                right = mid
            else:
                left = mid + 1
        
        return left