# 山脉数组的顶峰索引：https://leetcode-cn.com/problems/peak-index-in-a-mountain-array/

class Solution:
    def peakIndexInMountainArray(self, arr: list) -> int:
        """
            也可以抽象为二分查找， 但是要注意查找判断的条件是千变万化的
            这里是判断 mid 和 mid + 1谁和谁大。来缩小范围（因为mid = left + (right - left) // 2， 所以一定有mid, mid + 1,  不一定有 mid ， mid - 1），要注意细节。
        """
        left, right = 0, len(arr)

        while left < right:
            mid = left + (right - left) // 2
            # 说明还在上升
            if arr[mid] < arr[mid + 1]:
                left = mid + 1
            else:
            # 下降， mid 有可能是下降的最大元素（封顶），所以不能将它略过去
                right = mid
        
        return left