# 杨辉三角II： https://leetcode-cn.com/problems/pascals-triangle-ii/solution/

class Solution:
    def getRow(self, rowIndex: int):
        """ 只使用一个数组解决问题
            这个数组存放上一节的数字，然后一步步倒着推出来这一节的数字
            举例子 
            1 3 3 1   推出第五阶： 1 3 3 1 1 （后面补个1）
            倒着推 3 + 1 = 4， 3 + 3 = 6， 1 + 3 = 4. 依次倒着更新推出结果。得出结果：
            1 4 6 4 1
        """
        if rowIndex == 0:
            return [1]

        rst = [1, 1]             
        for i in range(1, rowIndex):
            rst.append(1)
            for j in range(len(rst)-2, 0, -1):
                rst[j] = rst[j] + rst[j-1]
        return rst    


# 小小的优化,去掉对 rowIndex == 0的特殊处理
class Solution:
    def getRow(self, rowIndex: int):
        """ 只使用一个数组解决问题
            这个数组存放上一节的数字，然后一步步倒着推出来这一节的数字
            举例子 
            1 3 3 1   推出第五阶： 1 3 3 1 1 （后面补个1）
            倒着推 3 + 1 = 4， 3 + 3 = 6， 1 + 3 = 4. 依次倒着更新推出结果。得出结果：
            1 4 6 4 1
        """
        if rowIndex == 0:
            return [1]

        rst = [1, 1]             
        for i in range(1, rowIndex):
            rst.append(1)
            for j in range(len(rst)-2, 0, -1):
                rst[j] = rst[j] + rst[j-1]
        return rst    


# 还可以有种静态的写法。 不使用 append()函数，每次往里面加入值
def getRow(self, rowIndex: int):
    oneRow = [0] * (rowIndex + 1)
    oneRow[0] = 1
    for row in range(1, rowIndex + 1):
        for col in range(row, 0, -1):
            oneRow[col] += oneRow[col - 1]

    return oneRow

