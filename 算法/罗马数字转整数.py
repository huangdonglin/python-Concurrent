# 罗马数字转整数：https://leetcode-cn.com/problems/roman-to-integer/
# 这道题要比 12号算法题： 整数转罗马数字还要简单一些。
class Solution:

    LM = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000
    }

    def romanToInt(self, s: str) -> int:
        """ 双指针解题,使用字典
            大的数一定在小的数前面。 如果小的数在大的数前面，那么比如 IV 就表示4， 可以理解为 -1 + 5 = 4 ,所以双指针，如果 前面所指，小于后面，那么就相当于减1
        """
        
        n = len(s)
        i = 0
        values = 0
        while i < n:
            j = i + 1
            if  i < n-1 and self.LM.get(s[i]) < self.LM.get(s[j]):
                values += -self.LM.get(s[i])                
            else:
                values += self.LM.get(s[i])
            i += 1

        return values



        