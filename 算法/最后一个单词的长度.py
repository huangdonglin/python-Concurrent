# 最后一个单词的长度： https://leetcode-cn.com/problems/length-of-last-word/

# 这道题太简单了，从后向前找就行，反转字符串中的单词III 本身就是简单题，这个更简单，是它解法的一部分
class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        n = len(s) - 1
        while n >= 0:
            # 排除字符串前后有空格的情况
            if s[n] != " ":
                start = n
                while start - 1 >= 0 and s[start - 1] != " ": start -= 1
                return n - start + 1 
            
            n -= 1

        return 0