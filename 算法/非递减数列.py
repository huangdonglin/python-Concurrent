# 非递减数列：https://leetcode-cn.com/problems/non-decreasing-array/

"""
虽然是道简单难度的题~ 但是还是不容易去想它的情况的! 这里总结一下
当 当前位置 i小于 i-1的时候， 有两个选择，
1. 要 更改 i位置的，
2. 还是 i - 1位置的。 
就像 4 5 3一样。 此时 i 指向 3。 5 3 改一个， 因为 3(i) < 4(i-2) 所以只能更改 3(i)
如果是 i > i-2 位置的值的话， 那么可以随意改 i -1


补充，为何下面的 if else 不能取相同逻辑
我感觉是可以随便修改的， 当 i < i - 1  且 i > i - 2

举例子 4 7 5， 当前i 为 5， 此时可以将 i 修改为 7， 也可以将 i - 1 修改为 5

那么问题来了， 按照取小值的 考虑、  应当将当前值变为 最小成立的值。再举例子
4 7 5 6  i

i 还是指向5 ，当 把 7变为 5， 4 5 5 6 成立， 
当把 5 变为 7，  4 7 7 6， 前三位是成立的，但是未必能满足 后续数字

因为 只考虑到 i-2, i-1, i三个数值，但是从长远考虑 ，应尽可能将数字改为最小符合要求的值

也就是只能有一种方案了，导致， if else 不能取相同逻辑

"""

class Solution:
    def checkPossibility(self, nums):
        
        # 可以改变的次数为 1
        count = 0
        i = 1
        # 当count > 1 或者 循环结束，退出
        while count <= 1 and i < len(nums):
            # 如果 i-1 小于 i 位置上面的值，就必须进行一次调整，调整还要分情况
            if nums[i] < nums[i-1]:
                # 可以改变 i 的值，还可以改变 i-1的值，分情况
                if i > 1 and nums[i] < nums[i-2]:
                    nums[i] = nums[i-1]
                else:
                    nums[i-1] = nums[i]
                count += 1 
            i += 1
        
        return count < 2