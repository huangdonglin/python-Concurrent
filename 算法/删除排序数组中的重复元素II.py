# 删除排序数组中重复元素II：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/submissions/

"""
快慢指针,  删除排序数组中重复元素的升级版，可先看简单的（26），再看这道题。
快指针 i：表示遍历的每个元素， 当前元素
慢指针 j：  j 之前的元素都是 判断过后，保留的。
j 到 i之间的 为判断后，不需要的
i 到 n 之间为 还没被判断的元素。
"""


class Solution:
    def removeDuplicates(self, nums):
        n = len(nums)
        """ 快慢指针 """
        # 因为可以重复一次，所以核心思想是从第三个元素开始。 第三个元素 不只要不和 前两个元素都相等，那么就可以添加
        i = j = 2
        while i < n:
            # 如果 nums 和前面两个数都相等，则放弃
            if (nums[i] == nums[j-1] and nums[j-2] == nums[i]):
                i += 1
            # 
            else:
                nums[j] = nums[i]
                i += 1
                j += 1

        # 去掉不要的数据，在后面 
        for k in range(j, len(nums)):
            nums.pop()
            
        return len(nums)



# 优化
class Solution:
    """ 优化if判断，代码更加简洁 """
    def removeDuplicates(self, nums):
        n = len(nums)
        """ 快慢指针 """
        # 因为可以重复一次，所以核心思想是从第三个元素开始。 第三个元素 不只要不和 前两个元素都相等，那么就可以添加
        i = j = 2
        while i < n:
            # 如果 nums 和前面两个数都相等，则放弃
            if not (nums[i] == nums[j-1] and nums[j-2] == nums[i]):
                nums[j] = nums[i]
                j += 1

            i += 1
                
        # 去掉不要的数据，在后面 
        for k in range(j, len(nums)):
            nums.pop()

        return len(nums)

