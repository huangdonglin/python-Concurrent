# 删除链表中的节点： https://leetcode-cn.com/problems/delete-node-in-a-linked-list/

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def deleteNode(self, node):
        """
        :type node: ListNode
        :rtype: void Do not return anything, modify node in-place instead.
        """
        """
            这题只知道 当前节点， 所以不能使用常规的删除， 要使后面节点的值，依次赋值到前面节点， 再删除最后一个节点。
        """
        # 表示当前节点和上一个节点
        cur = pre = node
        while cur.next is not None:
            cur.val = cur.next.val
            pre = cur
            cur = cur.next
        
        pre.next = None

