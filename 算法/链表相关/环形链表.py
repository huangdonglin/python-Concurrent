# 环形链表： https://leetcode-cn.com/problems/linked-list-cycle/

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


# 也是快慢指针做
class Solution:
    def hasCycle(self, head: ListNode) -> bool:
        """ 使用快慢指针，就像操场跑步，如果有环的话，快指针一定能追上慢指针 """
        low = fast = head
        # 如果节点数小于1 ，返回 False
        if not head or not head.next:
            return False

        # 假设快指针一次走两步，慢指针一次走一步
        while fast.next and fast.next.next:
            fast = fast.next.next
            low = low.next
            if low == fast:
                return True
        
        return False
