# 删除排序链表中的重复元素： https://leetcode-cn.com/submissions/detail/208045220/

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

# 这是我写的， 这样做不需要，重复的 进行删除操作
class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        """ 可以使用双指针， start 代表开始节点，  end 代表第一个不等于 start的节点"""

        if not head:
            return None
        end = start = head
        while end is not None:
            if end.val == start.val:
                end = end.next
            else:
                start.next = end
                start = end

        start.next = None

        return head


# 官网解法：
class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        if not head:
            return head

        cur = head
        while cur.next:
            if cur.val == cur.next.val:
                cur.next = cur.next.next
            else:
                cur = cur.next

        return head
