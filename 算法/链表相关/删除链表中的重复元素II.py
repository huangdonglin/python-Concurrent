# 删除；链表中的重复元素II：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list-ii/submissions/

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


# 以前做过这道题， 印象很深，竟然还是卡了很久
class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        """ 
            这道题， 删除所有重复的元素，那么头结点也可能重复，为了方便删除，必须
            添加首元结点， 来统一删除逻辑才方便
            同样的采用双指针，当后序出现的值，一直等于当前值， 就一起删掉
        """
        if not head:
            return head
        # 1. 添加首元结点
        dummy = ListNode(0, head)
        # 2. 循环先出
        pre = dummy
        cur = head
        while cur and cur.next:
            # 1. 如果相邻节点不等，就跳过
            if cur.val != cur.next.val:
                pre = cur
                cur = cur.next
            # 2. 相等，一直找到相等的最后一个元素，一起删除
            else:
                temp = cur.val
                # 循环找出所有值为 temp的节点
                while cur.next and cur.next.val == temp:
                    cur = cur.next
                
                pre.next = cur.next
                cur = cur.next

        head = dummy.next
        return head





