# 无重复字符的最长子串： 

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        """ 滑动窗口解题
            双指针用来确定窗口的边界。set集合类型，用来判断当前元素是否和窗口内元素重复
            原理是， j 向前走，碰到重复的， i 就移动到重复元素的 下一位，然后j再移动向前
            ，可以使用 字典代替 set， 记录元素和下标，从而快速移动到 重复元素下一位置
        """
        i = j = 0
        n = len(s)
        charSet = set()
        maxLength = 0
        while j < n:
            if s[j] not in charSet:
                charSet.add(s[j])
                j += 1
            else:
                maxLength = max(maxLength, j - i)
                # 将i移到重复元素下一位
                while s[i] != s[j]:
                    # i 向右走，并删除集合中当前元素，只保留在窗口中的
                    charSet.remove(s[i])
                    i += 1
                i += 1
                j += 1

        # 当j走到最后位置，可能还有一个子串长度, 注意当 j 走到最后，此时计算长度就变成了  j - i
        maxLength = max(maxLength, j - i)

        return maxLength

st = "tmmzuxt"

s = Solution()
rst = s.lengthOfLongestSubstring(st)
print(rst)