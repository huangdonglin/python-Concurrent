# 最大连续1个数II： https://leetcode-cn.com/problems/max-consecutive-ones-ii/（需要力扣会员）

"""
    题目描述： 在 I 的基础上， 可以将一个0变为1，然后统计 1的总和最大
"""

# 自己写的思路
class Solution:
    def findMaxConsecutiveOnes(self, nums: list) -> int:
        """
            思路： 还是使用双指针（也可以说是滑动窗口），默认只出现一个0，继续右移 j，
            直到出现第二个0 才去计算总和
        """
        n = len(nums)
        i = j = 0
        ans = 0
        # 记录 0的个数，第二个是记录0所在的下标
        windowZeroCnt = [0, -1]
        while j < n:
            # 1. 如果当前是0
            if nums[j] == 0:
                # 如果是第一个0， 标记一下
                if windowZeroCnt[0] == 0:
                    windowZeroCnt[0] += 1
                    windowZeroCnt[1] = j
                    j += 1
                # 如果第二次碰到0，计算和
                else:
                    ans = max(ans, j - i)
                    # i 移动到第一个0下一位
                    i = windowZeroCnt[1] + 1
                    windowZeroCnt[0] = 0
                    windowZeroCnt[1] = -1
            # 2. 如果当前是 1
            else:
                j += 1


        ans = max(ans, j - i)

        return ans


# 老师讲解的思路，确实更加简便， 而且这两种实现方式，可以记录 i的位置， 数据过大，不能全部加载进内存，同样可用
class Solution:
    def findMaxConsecutiveOnes(self, nums: list) -> int:
        n = len(nums)
        i = j = 0
        ans = 0
        # 记录 0 出现的下标，当下标不为 -1 时，说明是第二次出现0
        ZeroIndex = -1
        while j < n:
            # 1. 如果当前是0
            if nums[j] == 0:
                # 如果是第一个0， 标记一下
                if ZeroIndex == -1:
                    ZeroIndex = j
                # 如果第二次碰到0，计算和
                else:
                    ans = max(ans, j - i)
                    # i 移动到第一个0下标下一位
                    i = ZeroIndex + 1
                    # ZerIndex 更新为当前的 0位置
                    ZeroIndex = j
            j += 1

        ans = max(ans, j - i)

        return ans


# 再简化下上面的写法：
# 老师讲解的思路，确实更加简便， 而且这两种实现方式，可以记录 i的位置， 数据过大，不能全部加载进内存，同样可用
class Solution:
    def findMaxConsecutiveOnes(self, nums: list) -> int:
        n = len(nums)
        i = j = 0
        ans = 0
        # 记录 0 出现的下标，当下标不为 -1 时，说明是第二次出现0
        ZeroIndex = -1
        while j < n:
            # 1. 如果当前是0
            if nums[j] == 0:
                # 如果第二次碰到0，计算和
                if ZeroIndex != -1:
                    ans = max(ans, j - i)
                    # i 移动到第一个0下标下一位
                    i = ZeroIndex + 1

                # ZerIndex 更新为当前的 0位置
                ZeroIndex = j
                
            j += 1

        ans = max(ans, j - i)

        return ans


nums = [1, 0, 1, 1, 1, 0]

s = Solution()
rst = s.findMaxConsecutiveOnes(nums)
print(rst)

