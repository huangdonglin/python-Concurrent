# 最大连续1的个数: https://leetcode-cn.com/problems/max-consecutive-ones/solution/

# 自己写的思路
class Solution:
    def findMaxConsecutiveOnes(self, nums: list) -> int:
        """
            双指针： 1的区间为 [i, j]闭区间， 找到所有区间，看谁的大
            O(n), j指针遍历一次
        """
        n = len(nums)
        i = j = 0
        ans = 0
        while j < n:
            i = j
            while j < n and nums[j] == 0:
                i += 1
                j += 1
            while j < n and nums[j] == 1:
                j += 1

            ans = max(ans, j - i)
        
        return ans


# 这种思路可以解决 将 k 个 0变为 1的解法。 这里是 将 0 个 0变为 1， 具体可以看 变形 II， III 题
class Solution:
    def findMaxConsecutiveOnes(self, nums: list) -> int:
        """
            双指针
        """
        n = len(nums)
        i = j = 0
        ans = 0
        while j < n:
            if nums[j] == 0:
                ans = max(ans, j - i)
                i = j + 1

            j += 1
        # 可能最后的还没计算
        ans = max(ans, j - i)

        return ans