# 定长子串中元音的最大数目: https://leetcode-cn.com/problems/maximum-number-of-vowels-in-a-substring-of-given-length/

# 我的写法
class Solution:
    def maxVowels(self, s: str, k: int) -> int:
        """ 滑动窗口 """
        cnt = 0
        aeiouSet = set('aeiou')
        # 确定滑动窗口长度
        for j in range(k):
            if s[j] in aeiouSet:
                cnt += 1

        i = 0
        rst = cnt 
        for j in range(k, len(s)):
            # 1. 移动窗口
            if s[i] in aeiouSet:
                cnt -= 1
            i += 1
            if s[j] in aeiouSet:
                cnt += 1

            # 2. 计算最大值
            rst = max(rst, cnt)
            # 3. 最大值已经为窗口大小，直接返回
            if rst == k: return rst

        return rst


