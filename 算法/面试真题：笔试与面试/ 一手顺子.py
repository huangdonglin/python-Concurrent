# 一手顺子： https://leetcode-cn.com/problems/hand-of-straights/

class Solution:
    def isNStraightHand(self, hand: List[int], groupSize: int) -> bool:
        # 使用 Counter类进行 计数, 初始化 hand
        count = collections.Counter(hand)
        
        while count:
            m = min(count)
            for k in range(m, groupSize + m):
                v = count[k]
                if not v: return False
                if v == 1:
                    del count[k]
                else:
                    count[k] = v - 1

        return True

        