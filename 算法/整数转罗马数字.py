# 整数转罗马数字: https://leetcode-cn.com/problems/integer-to-roman/

class Solution:
    def intToRoman(self, num: int) -> str:
        """
            有点使用贪心的感觉。 将这些对应的罗马数字和 整数之间对应起来（使用两个数组或者是元组）， 因为大的始终在小的右边， 我们优先计算大的数
        """
        # python的解法，元组更为合适。 不看老师讲解，我一直在考虑怎么联系这两个。起初是想一个数组加上一个字典的。代码很简单，但是想到这样写，思路需要很清晰。
        VALUE_SYMBOLS = [
        (1000, "M"),
        (900, "CM"),
        (500, "D"),
        (400, "CD"),
        (100, "C"),
        (90, "XC"),
        (50, "L"),
        (40, "XL"),
        (10, "X"),
        (9, "IX"),
        (5, "V"),
        (4, "IV"),
        (1, "I"),
        ]

        i, rst = 0, ''
        while i < 13:
            if VALUE_SYMBOLS[i][0] <= num:
                rst += VALUE_SYMBOLS[i][1]
                num -= VALUE_SYMBOLS[i][0]
            else:
                i += 1

        return rst





