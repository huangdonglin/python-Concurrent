# 外观数列： https://leetcode-cn.com/problems/count-and-say/

class Solution:
    def countAndSay(self, n: int) -> str:
        # 初始置为 "1"
        rst = "1"
        # 循环一次求，每次的rst(外观)
        for i in range(n-1):
            left = right = 0
            temp, length = '', len(rst)
            # 具体求一次外观，两层循环
            while right < length:
                # 左右指针，寻找重复的字符左右边界。 count计数
                count = 0
                while right < length and rst[left] == rst[right]:
                    count += 1; right += 1
                temp += str(count) + rst[left]
                # left作为下次字符验证的开始，赋值到right处。每次记完一个字符出现的次数，就要清零，重新计算下一个字符个数
                left = right; 

            # rst 迭代为这一次的外观
            rst = temp

        return rst


s = Solution()
rst = s.countAndSay(5)
print(rst)



