# 反转字符串II： https://leetcode-cn.com/problems/reverse-string-ii/

class Solution:
    def reverseStr(self, s: str, k: int) -> str:
        s = list(s)
        """
            直接模拟过程, 也要注意字符串转换为 字符数组，字符串是不可变对象
        """
        n = len(s)
        step = n // (k * 2)
        remaining = n % (k*2)

        # 实现一个求反转的函数
        def reverse(left, right):
            while left < right:
                s[left], s[right] = s[right], s[left]
                left += 1; right -= 1

        # 处理分组的
        for i in range(step):
            left, right = i*2*k, i*2*k + k - 1
            reverse(left, right)
        
        # 处理剩下的
        if remaining > k: 
            left, right = 2*k*step, 2*k*step + k - 1
            reverse(left, right)
        else: 
            left, right = 2*k*step, len(s)-1
            reverse(left, right)

        return ''.join(s)


# 更优雅的写法： 不需要对尾部情况，另加代码特殊处理，值得学习。
class Solution:
    def reverseStr(self, s: str, k: int) -> str:
        s = list(s)
        """
            直接模拟过程
        """
        # 实现一个求反转的函数
        def reverse(left, right):
            while left < right:
                s[left], s[right] = s[right], s[left]
                left += 1; right -= 1

        # 处理分组的,利用取步长，不需要在进行特殊处理尾部
        for start in range(0, len(s), 2*k):
            # 一般是right = left + k - 1, 但是如果到最后了，不够 k长，那么 right = len(s) - 1。 这样就处理了尾部了~！ 
            left, right = start, min(start + k - 1, len(s) - 1)
            reverse(left, right)
        
        return ''.join(s)


# 最优解，函数式编程。一句话解决~ 且执行效率用时超过 98.50%的用户32ms。 前面两种方法都是执行100多ms
class Solution:
    def reverseWords(self, s: str) -> str:
        # 函数式编程
        return " ".join(word[::-1] for word in s.split(" "))

        