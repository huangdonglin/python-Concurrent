# https://leetcode-cn.com/problems/excel-sheet-column-title/

"""
    绕弯就在于这道题是 1-26， 进制不是 0-25
"""


# 思路一
class Solution:
    def convertToTitle(self, columnNumber: int) -> str:
        """ 转化为求26进制数对应的十进制数 """

        rst = ''
        while columnNumber > 0:
            # 求余数
            columnNumber -= 1
            yu = columnNumber % 26
            # 余数转换为对应的 字母并加入结果里
            rst = chr(yu + ord('A')) + rst

            columnNumber = columnNumber // 26

        return rst


# 解法思路二
class Solution:
    def convertToTitle(self, columnNumber: int) -> str:
        """ 转化为求26进制数对应的十进制数 """

        rst = ''
        while columnNumber > 0:
            # 求余数
            yu = columnNumber % 26
            if yu == 0:
                yu = 26
            # 余数转换为对应的 字母并加入结果里
            rst = chr(yu + ord('A') -1) + rst

            # 1-26的数字就结束了， 属于“个位”，  -1 ，不在进行下一轮
            columnNumber = (columnNumber-1) // 26

        return rst

    # 701 % 26 = 25 --> Y  ， (701 - 1) // 26 = 26
    # 26 % 26 = 0 --> Z(相当于26)
    # (26 - 1) // 26 = 0 --> 结束