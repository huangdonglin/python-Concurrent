# 汇总区间：https://leetcode-cn.com/problems/summary-ranges/

class Solution: 
    def summaryRanges(self, nums):
        n = len(nums)
        left = right = 0
        rst = list()

        """
        思路：两个指针，一个标记开头，一个标记结尾。 难点在于对 最后元素的处理，比方说 right = n-1时
        """
        while right < n:
            # 如果 right + 1的元素连续，则right + 1
            # 注意 right < n-1, 因为后面有 right + 1， 且遍历到 最后一个元素时，必须交由 else的逻辑处理，添加进 rst
            if right < n-1 and nums[right] + 1 == nums[right + 1]:
                right += 1
            # 不连续就将现在的left，right 加进去rst
            else:
                # 只有一个元素
                if left == right:
                    rst.append(str(nums[left]))
                # 超过一个元素连续
                else:
                    rst.append(str(nums[left])+"->"+str(nums[right]))
                right = right + 1
                left = right

        return rst
        

# 优化if判断写法之后的：
class Solution:
    def summaryRanges(self, nums):
        n = len(nums)
        left = right = 0
        rst = list()

        """
        思路：两个指针，一个标记开头，一个标记结尾。 难点在于对 最后元素的处理，比方说 right = n-1时
        """
        while right < n:
            if not (right < n-1 and nums[right] + 1 == nums[right + 1]):
                if left == right:
                    rst.append(str(nums[left]))
                else:
                    rst.append(str(nums[left])+"->"+str(nums[right]))
                left = right + 1
            right += 1
            
        return rst
