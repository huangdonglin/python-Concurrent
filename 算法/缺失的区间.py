# 缺失的区间：力扣162号算法题

def finfMissingRanges(nums, lower, upper):
    """
    因为 lower < min(nums) , max(nums) < upper
    1. 所以只需要求出 每相邻两个值之间的数据就可以了。
    2. 需要注意的是，两边的 lower， 和 upper 是不一样的处理逻辑。因为他们作为边界是可以取到的。
    所以要修改下 lower 边界的值变为lower-1，使他们也被正常处理，不需要单独考虑
    """
    # pre 指向开始的值
    pre = lower - 1
    rst = list()
    for i in range(len(nums)):
        sub = nums[i] - pre
        if sub == 2:
            rst.append(str(nums[i]-1))
        if sub > 2:
            rst.append(str(pre+1)+"->"+str(nums[i]-1))
        pre = nums[i]
    
    # 还剩一个 upper没有处理
    if upper == pre + 1:
        rst.append(str(upper))
    elif upper > pre + 1:
        rst.append(str(pre+1)+"->"+str(upper))
    
    return rst

# 优化写法，1. 对 lower，和 upper 都不做特殊处理 2. 优化for循环
def finfMissingRanges(nums, lower, upper):
    pre = lower - 1
    nums.append(upper+1)
    rst = list()
    for num in nums:  # 不需要索引，只需要值
        sub = num - pre
        if sub == 2:
            rst.append(str(num-1))
        elif sub > 2:
            rst.append(str(pre+1)+"->"+str(num-1))
        pre = num
    nums.pop()
    return rst

case1 = [0, 1, 3, 50, 75]
s = finfMissingRanges(case1, 0, 99)
print(s)
x = finfMissingRanges(case1, -1, 76)
print(x)
