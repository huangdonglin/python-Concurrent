# 螺旋矩阵II ： https://leetcode-cn.com/problems/spiral-matrix-ii/

class Solution:
    def generateMatrix(self, n: int):
        """
            参考54题，螺旋矩阵
        """
        total = n**2
        rst = [[0 for i in range(n)] for j in range(n)]
        rst[0][0] = 1
        step = 1
        row = col = top = 0
        while step < total:
            while col + 1 < n and step < total:
                rst[row][col + 1] = step + 1
                col += 1
                step += 1
            
            while row + 1 < n and step < total:
                rst[row + 1][col] = step + 1
                step += 1
                row += 1
            
            while col - 1 >= top and  step < total:
                rst[row][col - 1] = step + 1
                step += 1
                col -= 1
            
            top += 1
            while row -1  >= top and step < total:
                rst[row - 1][col] = step + 1
                step += 1
                row -= 1

            n -= 1

        return rst 

s = Solution()
rst = s.generateMatrix(4)
print(rst)








