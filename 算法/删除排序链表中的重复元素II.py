class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

a = ListNode(1)
a.next = ListNode(2)
a.next.next = ListNode(3)
a.next.next.next = ListNode(3)
a.next.next.next.next = ListNode(4)
a.next.next.next.next.next = ListNode(4)
a.next.next.next.next.next.next = ListNode(5)


class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        if not head:
            return head
        
        dummy = ListNode(0, head)

        cur = dummy

        while cur.next and cur.next.next:
            if cur.next.val == cur.next.next.val:
                x = cur.next.val
                while cur.next and cur.next == x:
                    cur.next = cur.next.next
            else:
                cur = cur.next

        return dummy.next

so = Solution()
result = so.deleteDuplicates(a)
print(result)


# class Solution:
#     def deleteDuplicates(self, head: ListNode) -> ListNode:
#         pre = head
#         flag = 0

#         # 判断出头结点
#         while pre.next:
#             if pre.val == pre.next.val:
#                 flag = 1
#                 pre.next = pre.next.next
#             else:
#                 pre = pre.next
#                 if flag == 1:
#                     head = pre
#                     flag = 0
#                 else:
#                     break
#         print(pre.val)
#         print(head.val)
#         # 向后删除重复节点
#         p_pre = head
#         flag1 = 0
#         while pre.next:
#             if pre.val == pre.next.val:
#                 flag1 = 1
#                 pre.next = pre.next.next
#             else:
#                 if flag1 == 1:
#                     pre = pre.next
#                     p_pre.next = pre
#                     flag = 0
#                 else:
#                     pre = pre.next
#                     p_pre = p_pre.next
        
#         return head
    

