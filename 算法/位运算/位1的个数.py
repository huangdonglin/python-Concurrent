# 位1的个数：https://leetcode-cn.com/problems/number-of-1-bits/

# 解法1
class Solution:
    def hammingWeight(self, n: int) -> int:
        """
            对每一位进行测试，看是否是1  利用 >> 右移运算符还有 &， 使n的每一位都与1相与
        """
        count = 0
        for i in range(32):
            if (n >> i) & 0x00000001 == 1:   # 可以直接写为1
                count += 1

        return count




# 解法2： 
class Solution:
    def hammingWeight(self, n: int) -> int:
        """
            对每一位进行测试，看是否是1  利用 << 左移运算符还有 &， 使n的每一位都与1相与
        """
        count = 0
        for i in range(32):
            if n & (1 << i):   # 得到的结果就不是1了，不能使用 == 1来判断
                count += 1

        return count



# 解法3：
# 观察这个运算：n & (n - 1)，其运算结果恰为把 n 的二进制位中的最低位的 1 变为 0 之后的结果
class Solution:
    def hammingWeight(self, n: int) -> int:
        count = 0
        while n != 0:
           n = n & (n-1) 
           count += 1

        return count

