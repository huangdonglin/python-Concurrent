# 只出现一次的数字II：https://leetcode-cn.com/problems/single-number-ii/submissions/

# python使用 map的三种解决方案
import collections

# 1. 普通字典
class Solution:
    def singleNumber(self, nums) -> int:
        """
            hash查找解决
        """
        map = {}
        for num in nums:
            if not map.get(num, None):
                map[num] = 0
            map[num] += 1
        
        for key,val in map.items():
            if val == 1:
                return key


# 2.defaultdict
class Solution:
    def singleNumber(self, nums) -> int:
        """
            hash查找解决
        """
        from collections import defaultdict

        map = defaultdict(int)

        for num in nums:
            map[num] += 1
        
        for key,val in map.items():
            if val == 1:
                return key




# 3.最优
def singleNumber(self, nums) -> int:
    """
        hash查找解决
    """
    freq = collections.Counter(nums)
    ans = [num for num, occ in freq.items() if occ == 1][0]
    return ans