# 只出现一次的数字： https://leetcode-cn.com/problems/single-number/

class Solution:
    def singleNumber(self, nums) -> int:
        """ 抓住两条性质
            a ^ 0 = a    作用： 可以统计列表中只出现一次的元素（其他元素出现两次）
            a ^ a = 0
        """
        ans = 0
        for num in nums:
            ans ^=  num

        return ans
