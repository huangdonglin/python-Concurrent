# 2的幂： https://leetcode-cn.com/problems/power-of-two/

# 暴力破解
class Solution:
    def isPowerOfTwo(self, n: int) -> bool:
        if n == 0: return False

        while (n % 2) == 0: 
            n /= 2
            
        return True if n == 1 else False

# 优化解法
class Solution:
    def isPowerOfTwo(self, n: int) -> bool:
        """
            找到2的幂的规律： 只要是2的幂次方，肯定二进制中只有一个1！
            那么只要这个数字去掉一个1，就一定是 0！
        """
        return n > 0 and (n & (n - 1)) == 0