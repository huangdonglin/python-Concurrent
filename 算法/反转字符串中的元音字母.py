# 反转字符串中的元音字母: https://leetcode-cn.com/problems/reverse-vowels-of-a-string/

class Solution:
    def reverseVowels(self, s: str) -> str:
        yuan = {'a': 0, 'e': 0, 'i': 0, 'o': 0, 'u': 0}
        """
            和列表反转是一样的，但是需要注意的是，字符串是不可变变对象， 使用下标 如 s[1] = "c" 会报错。先转换为列表
        """
        s = list(s)
        left, right = 0, len(s)-1
        while left < right:
            # 找到元音字母,大小写都算
            while s[left].lower() not in yuan and left < right: left += 1
            while s[right].lower() not in yuan and left < right: right -= 1
            
            # 做交换
            s[left], s[right] = s[right], s[left]
            left += 1; right -= 1

        return ''.join(s)


s1 = "abcdifu"

s = Solution()
print(s.reverseVowels(s1))


