# 螺旋矩阵： https://leetcode-cn.com/problems/spiral-matrix/

# 完全是个人思路编写，只介绍这一种解法。 还有使用列表模拟步骤的方法
class Solution:
    def spiralOrder(self, matrix):
        """ 直接模拟
            通过顺时针的方式，模拟这个过程
            1. 顺时针时，col， row 的遍历不能超过 n, m。 且向上，向左时， col ， row  > top(顶端) 。 n ,m 的值每次都会减少，因为外圈遍历过了。 top 每次增加，原理是一样的，外圈遍历过
            2. 
            3. 技巧：把matrix[0][0]事先加入进去。 判断 row + 1 < m, 或者 col + 1 < n. 因为这样得到， row， col 恰好是最后一个位置。 而如果判断 row < n, 此时col已经超出 列表长度了， 比较麻烦！
        """
        # m行 n列
        m, n = len(matrix), len(matrix[0])
        total = n * m
        # rst结果集列表
        rst = []
        rst.append(matrix[0][0])
        # 计数，添加几个元素进去了，默认为1，添加一个开始元素进去
        step = 1
        # 当前，行列 默认值为 0.
        row = col = 0
        top = 0

        # 当计数个数达到 n*m 说明所有元素都进去了。那么就结束循环
        while step < total:
            # 直着向右
            while col + 1 < n and step < total:
                rst.append(matrix[row][col + 1])
                col += 1
                step += 1
            
            # 直着向下
            while row + 1 < m  and step < total:
                rst.append(matrix[row + 1][col])
                row += 1
                step += 1

            # 直着向左
            while col - 1 >= top and step < total:
                rst.append(matrix[row][col - 1])
                col -= 1
                step += 1

            # 直着向上
            top += 1
            while row - 1 >= top and step < total:
                rst.append(matrix[row - 1][col])
                row -= 1
                step += 1
            n -= 1
            m -= 1

        return rst


# matrix = [[1,2,3],[4,5,6],[7,8,9]]
# matrix = [
#     [23,18,20,26,25],
#     [24,22,3,4,4],
#     [15,22,2,24,29],
#     [18,15,23,28,28]]
matrix = [
    [1,2,3,4],
    [5,6,7,8],
    [9,10,11,12]
]

s = Solution()
rst = s.spiralOrder(matrix)
print(rst)




            
        



