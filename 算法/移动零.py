# 移动零：https://leetcode-cn.com/problems/move-zeroes/

def moveZeroes(nums: list) -> None:
    """
    Do not return anything, modify nums in-place instead.
    """

    """
        快慢指针，快指针用于 遍历找到非零元素， 慢指针用于指向需要和非零元素交换的 那个 0元素
    """
    i = j = 0
    while i < len(nums):
        if nums[i] != 0 :
            
            nums[j] = nums[i]
            j += 1
        i += 1

    # 优化，赋值代替交换，提升效率
    for k in range(j,len(nums)):
        nums[k] = 0
    
    return nums


moveZeroes([1,3,12,0,0])
