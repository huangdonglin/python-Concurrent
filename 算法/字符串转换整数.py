# 字符串转换整数: https://leetcode-cn.com/problems/string-to-integer-atoi/

# 自己写的，按照步骤，直接模拟
class Solution:
    def myAtoi(self, s: str) -> int:
        n = len(s)
        i, rst, sign = 0, '', True
        while i < n:
            # 去掉前导空格
            if s[i] != " ":
                if s[i] == "-": 
                    i += 1
                    sign = False
                elif s[i] == "+":
                    i += 1
                # 是数字或者是字母

                # 非数字, 这里i >= n，如果 字符串只是 “+” 那么这里的操作，索引会越界。 判断下，越界节结束了
                if i >= n or ord(s[i]) < ord("0") or ord(s[i]) > ord("9"):
                    return 0
                else:
                    while i  < n and ord(s[i]) >= ord("0") and ord(s[i]) <= ord("9"):
                        rst += s[i]
                        i += 1
                # 加入符号
                rst = int(rst) if sign else int(rst)*-1
                # 是否超过32位有符号范围
                if -2**31 > rst:
                    rst = -2**31
                elif rst > 2**31 - 1:
                    rst = 2**31 -1
                return rst
            i += 1

        return 0


# 优化, 简化代码写法
class Solution:
    def myAtoi(self, s: str) -> int:
        n = len(s)
        i, rst, sign = 0, 0, 1
        while i < n:
            # 1. 去掉前导空格
            if s[i] != " ":
                # 2. 判断符号: sign 代表符号，默认为1，正
                if s[i] == "-" or s[i] == "+": 
                    sign = -1 if s[i] == "-" else 1
                    i += 1

                # 3. 如果i > n 说明符号后面没有内容了
                if rst >= n: return 0
                # 4.取数字，并且合并。 如果不是数字， 那么默认 rst = 0
                while i < n and ord(s[i]) >= ord("0") and ord(s[i]) <= ord("9"):
                        # 5. 是否超过32位有符号范围, 要放到这里来，因为对于其他一些语言， int型， long型。 如果数字过大，不能一直去计算下去。当要超过时，就返回
                    if rst > 2**31 // 10 or (rst == 2**31 // 10 and ord(s[i]) - ord("0") > 7):  
                        if sign == 1: return 2**31 -1 
                        else:
                            return -2**31        
                    # 累加求数
                    rst = rst*10 + ord(s[i]) - ord("0")
                    i += 1
                return sign * rst 
            i += 1
        return 0

str1 = "4193 with words"

s = Solution()
rst = s.myAtoi(str1)
print(rst)
