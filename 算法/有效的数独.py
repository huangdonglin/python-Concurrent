# 有效的数独： https://leetcode-cn.com/problems/valid-sudoku/


class Solution:
    def isValidSudoku(self, board):
        """
            核心是：9x9的数独，元素也是 1-9 等于索引下标。 所以用下标进行计算！ 当该下标位置已经被改为True，说明该(下标+1)的数字已经存在
            计算元素所在的九宫格块， 竖着数 分别是1,2,3,4,5,6,7,8,9  块数也对应着判断重复数组对应的行数。计算公式为：box_index = row//3 + (col//3)*3
        """
        # 用来判断行是否重复元素
        rows = [[False for j in range(9)] for i in range(9)]
        # 用来判断列是否有重复元素
        cols = [[False for j in range(9)] for i in range(9)]
        # 按断九宫格是否有重复元素
        box = [[False for j in range(9)] for i in range(9)]
        
        for row in range(9):
            for col in range(9):
                if board[row][col] == '.':
                    continue
                # 计算下标索引
                index = int(board[row][col]) - 1
                # 计算钙元素所在的九宫格块， 竖着数 
                box_index = row//3 + (col//3)*3
                # 判断三者当前位置，是否已经是True（是否已经出现过下标加1的数）
                if rows[row][index] == True or cols[col][index] == True or box[box_index][index] == True:
                    return False
                rows[row][index] = True
                cols[col][index] = True
                box[box_index][index] = True

        return True
