# 比较版本号： https://leetcode-cn.com/problems/compare-version-numbers/

class Solution:
    def compareVersion(self, version1: str, version2: str) -> int:
        """
            不使用内置函数的写法。 直接模拟
            比较逗号之前的，一组一组的比较。转化为整数
        """
        len1, len2 = len(version1), len(version2)
        # 定义两个指针
        p1 = p2 = 0
        while p1 < len1 or p2 < len2:
            # 定义两个比较的整数
            v1 = v2 = 0
            while p1 < len1 and version1[p1] != ".":
                v1 = v1 * 10 + ord(version1[p1]) - ord("0")
                p1 += 1

            while p2 < len2 and version2[p2] != ".":
                v2 = v2 * 10 + ord(version2[p2]) - ord("0")
                p2 += 1
            
            if v1 > v2 :
                return 1
            elif v1 < v2:
                return -1
            p1 += 1; p2 += 1
        
        return 0


# ver1 = "1.01"
# ver2 = "1.001"
ver1 = "0.1"
ver2 = "1.1"

s = Solution()
rst = s.compareVersion(ver1, ver2)
print(rst)



