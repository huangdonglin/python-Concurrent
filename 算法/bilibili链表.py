class Node():
    def __init__(self, val = 0, next = None):
        self.val = val
        self.next = next


# data = input().split(",")

data = [1, 2, 3, 4 ,5]

def getLink(data):
    # 建立首元结点
    dumpy = Node()
    cur = dumpy
    # 建立链表
    for i in range(len(data)):
        cur.next = Node(int(data[i]))
        cur = cur.next

    # 将后面的链表进行反转， 使用快慢指针
    low = fast = dumpy.next
    while fast.next and fast.next.next:
        fast = fast.next.next
        low = low.next

    # 拆分成两个链表
    leftHead = dumpy.next
    rightHead = low.next
    low.next = None

    # 将后面的链表翻转
    pre = None
    cur = rightHead
    while cur:
        temp = cur
        cur = cur.next
        temp.next = pre
        pre = temp

    # 新的头节点
    rightHead = pre



    # 实现拼接
    cur = rightHead
    pre = leftHead
    while cur and pre:
        temp = cur
        cur = cur.next

        temp.next = pre.next
        pre.next = temp

        pre = pre.next.next

    # 最后遍历返回重组后的链表
    cur = leftHead
    while cur:
        if cur.next:
            print(cur.val, end = ",")
        else:
            print(cur.val, end = "")
        cur = cur.next


getLink(data)



