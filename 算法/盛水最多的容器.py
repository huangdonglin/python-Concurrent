# 盛水最多的容器： https://leetcode-cn.com/problems/container-with-most-water/solution/

# 对撞指针， 和正确性分析（力扣评论内容）
class Solution:
    def maxArea(self, height):
        """ 
        感觉这个移动有点博弈论的味了，每次都移动自己最差的一边，
        虽然可能变得更差，但是总比不动（或者减小）强，动最差的部分可能找到更好的结果，
        但是动另一边总会更差或者不变
        """

        n = len(height)
        low, high = 0, n - 1
        ans = 0
        while low < high:
            area = min(height[low], height[high]) * (high - low)
            ans = max(area, ans)
            if height[low] <= height[high]:
                low += 1
            else:
                high -= 1
        
        return ans

