# 子集：https://leetcode-cn.com/problems/subsets/

class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        """
            就是求所有组合，也不需要剪枝
        """
        rst = []
        def getSubSets(arr, idx):
            rst.append(arr[:])
            for i in range(idx, len(nums)):
                arr.append(nums[i])
                getSubSets(arr, i + 1)
                arr.pop()

        getSubSets([], 0)
        return rst

