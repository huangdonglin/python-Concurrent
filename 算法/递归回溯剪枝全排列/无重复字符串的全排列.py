# 无重复字符串的全排列: https://leetcode-cn.com/problems/permutation-i-lcci/

class Solution:
    def permutation(self, S: str) -> list:
        """
            可以利用树形结构写个递归解决全排列
        """
        res = []
        ## 想法一使用回溯法
        def dfs(path):
            if len(path) == len(S):
                res.append(path)
                return 
            for i in S:
                if i in path: continue
                dfs(path + i)
        
        dfs('')
        return res

''' 

                    ''
              q     w     e
           w   e  q   e  q   w
        e       we     q w     q 

          0  
        a   b
       b     a
'''

        