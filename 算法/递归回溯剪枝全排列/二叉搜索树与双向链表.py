# 二叉搜索树与双向链表: https://leetcode-cn.com/problems/er-cha-sou-suo-shu-yu-shuang-xiang-lian-biao-lcof/


# Definition for a Node.
class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
        
class Solution:
    # 定义头结点
    head = None

    def treeToDoublyList(self, root: 'Node') -> 'Node':
        """
            二叉排序树的中序遍历，就是有序的单链表，只需要再记录一下对应的 上一个节点就可以了
        """ 
        # 记录当前节点的上一个节点，需要是非局部变量，它的值是根据递归到某个节点动态改变的，且回溯到上一层结果不能变代表当前节点的上一个节点
        # 空链表直接返回
        if not root: return None

        pre = None
        def gDoubleLink(cur):
            """ 递归求解 """
            nonlocal pre   
            if not cur: return 
            
            # 处理左边
            gDoubleLink(cur.left)
            # 处理中间，如果有pre 说明不是首元结点。正常操作双指针
            if pre:
                cur.left = pre
                pre.right = cur
            else:
                # 这里只执行一次，没有pre指向的就是第一个节点，head指向它
                self.head = cur
            # 注意！： 前一个节点和当前节点串起来之后，就让当前节点变成前一个节点。 大家可以把递归最小子问题想出来，只有三个节点。就可以想到要在 right之前 更新pre了
            pre = cur
            # 执行右节点
            gDoubleLink(cur.right)
          

        gDoubleLink(root)
        # 首尾相连， 最后pre指针指向的是 最后一个元素
        pre.right = self.head
        self.head.left = pre 

        return self.head