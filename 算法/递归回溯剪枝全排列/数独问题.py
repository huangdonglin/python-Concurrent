# 数独问题：https://leetcode-cn.com/problems/sudoku-solver/submissions/

class Solution:
    def solveSudoku(self, board: List[List[str]]) -> None:
        """
            这道题我没有自己去写，这是复制题解的。 和 n 皇后一样， 判断每行，每列，“每个九宫格”约束。
            麻烦点在于，对着三个约束条件的判断。 使用多维数组表示。
        """
        def dfs(pos: int):
            nonlocal valid
            if pos == len(spaces):
                valid = True
                return
            
            i, j = spaces[pos]
            for digit in range(9):
                if line[i][digit] == column[j][digit] == block[i // 3][j // 3][digit] == False:
                    line[i][digit] = column[j][digit] = block[i // 3][j // 3][digit] = True
                    board[i][j] = str(digit + 1)
                    dfs(pos + 1)
                    line[i][digit] = column[j][digit] = block[i // 3][j // 3][digit] = False
                if valid:
                    return
            
        line = [[False] * 9 for _ in range(9)]
        column = [[False] * 9 for _ in range(9)]
        block = [[[False] * 9 for _a in range(3)] for _b in range(3)]
        valid = False
        spaces = list()

        for i in range(9):
            for j in range(9):
                if board[i][j] == ".":
                    spaces.append((i, j))
                else:
                    digit = int(board[i][j]) - 1
                    line[i][digit] = column[j][digit] = block[i // 3][j // 3][digit] = True

        dfs(0)


linux 文件目录结构

bin： 存放可执行的二进制文件， 常用的命令一般在这里。比如 ls ， cp等
etc：存放一些配置文件。 系统管理以及配置文件都在这个目录下。 有很多以 .conf结尾的配置文件
home： 每个用户的 根目录，是用户的主目录。
usr： 放的是一些系统应用目录。 usr/local 比较重要：存放管理安装的软件的目录。
    usr/local/bin: 是安装的一些软件的 二进制文件。
    usr/local/etc: 是用户安装软件的配置文件。
    usr/local/lib， include 安装软件所需要的一些 动态库 和文件等。

proc: 存放的是虚拟文件系统的目录。系统内存的映射。里面的数字文件夹，就是对应的一些进程。
dev： linux的设备文件。鼠标，输出等等。
boot： 存放系统引导时使用的各个文件
lib： 系统启动或者运行时，使用的动态库文件。

相对路径： 