class Solution:
    def lemonadeChange(self, bills):
        five = ten = twenty = 0
        if bills[0] != 5:
            return False
        for i in range(len(bills)):
            if bills[i] == 5:
                five += 1
            elif bills[i] == 10:
                if five > 1:
                    five -= 1
                    ten += 1
                else:
                    return False
            else:
                if ten > 1 and five >= 1:
                    ten -= 1
                    five -= 1
                    twenty += 1
                elif five > 3:
                    five -= 3
                    five += 1
                else:
                    return False
        else:
            return True

nums = [5,5,5,5,10,5,10,10,10,20]

s = Solution()
s.lemonadeChange(nums)