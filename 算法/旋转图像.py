# 旋转图像：https://leetcode-cn.com/problems/rotate-image/

# 方案一： 利用同样大小的 nXn矩阵计算，找到行列的规律

class Solution:
    def rotate(self, matrix):
        """
        Do not return anything, modify matrix in-place instead.
        """
        n = len(matrix)
        l = [[0 for j in range(n)] for i in range(n)]

        for i in range(n):
            for j in range(n):
                a = matrix[i][j]
                l[j][n-1-i] = a

        for i in range(n):
            matrix[i] = l[i]
            
        """
        row --> 列 n-row
        col --> row 
        0, 0 -->   0 , 3 
        0, 1 -->  1, 3
        """

# 原地旋转
class Solution:
    def rotate(self, matrix):
        """
        Do not return anything, modify matrix in-place instead.

        """
        n = len(matrix)
        for row in range(n//2):
            for col in range((n+1)//2):
                # 记录起始元素值
                temp = matrix[row][col]
                matrix[row][col] = matrix[n-col-1][row]
                matrix[n-col-1][row] = matrix[n-row-1][n-col-1]
                matrix[n-row-1][n-col-1] = matrix[col][n-row-1]
                matrix[col][n-row-1] = temp

        return matrix      

        
# 原地翻转
class Solution:
    def rotate(self, matrix):
        """
        Do not return anything, modify matrix in-place instead.
        原地翻转
        """
        n = len(matrix)
        for row in range(n//2):       
            for col in range(n):
                matrix[row][col], matrix[n-row-1][col] = matrix[n-row-1][col], matrix[row][col]

        for row in range(n):
            for col in range(row + 1, n):
                matrix[row][col], matrix[col][row] = matrix[col][row], matrix[row][col]
        
        return matrix
        


        



