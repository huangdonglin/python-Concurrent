# 同构字符串： https://leetcode-cn.com/problems/isomorphic-strings/

class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        """
            就是简单的顺序映射，s的每一位置上的，映射t上的每一个。 当已有映射不等时，返回错误
            例如： s = "foo", t = "bar"， 每一位对应， o 不能映射为 a，又映射为 r
        """
        STMap = dict()
        TSMap = dict()
        for i in range(len(s)):
            if s[i] not in STMap:
                STMap[s[i]] = t[i]
            else:
                if STMap[s[i]] != t[i]:
                    return False

        # 要满足双射，也就是 s - t, t - s 都是不能重复的映射
        for i in range(len(s)):
            if t[i] not in TSMap:
                TSMap[t[i]] = s[i]
            else:
                if TSMap[t[i]] != s[i]:
                    return False
        return True


# 优化： 双循环可以写成一个
class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        """
            就是简单的顺序映射，s的每一位置上的，映射t上的每一个。 当已有映射不等时，返回错误
            例如： s = "foo", t = "bar"， 每一位对应， o 不能映射为 a，又映射为 r
        """
        STMap = dict()
        TSMap = dict()
        for i in range(len(s)):
            # s -> t
            if s[i] in STMap and STMap[s[i]] != t[i]:
                return False
            # t -> s 
            if t[i] in TSMap and TSMap[t[i]] != s[i]:
                return False
            # 进一步可以优化成一句： () or () 用or 连接:
            # (s[i] in STMap and STMap[s[i]] != t[i]) or (t[i] in TSMap and TSMap[t[i]] != s[i])
            STMap[s[i]] = t[i]
            TSMap[t[i]] = s[i]

        return True