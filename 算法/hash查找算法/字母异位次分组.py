# 字母异位词分组： https://leetcode-cn.com/problems/group-anagrams/

class Solution:
    def groupAnagrams(self, strs: list) -> list:
        """
            普通的进行比较一定会超时的，因为数组个数很大，所以只能往 hash搜索上想，消耗空间换时间
            字母异位，也就是说，都是由相同的字符组成， 可以将这些字符按照26个字母顺排序当做 key， 对应的由这些字母组成的当成value
        """

        wordMap = {}
        # 遍历每一个单词
        for word in strs:
            # 可以用 sorted 优化，写成一行
            wordList = list(word)
            wordList.sort()
            key = ''.join(wordList)
            wordMap.setdefault(key, []).append(word)
        
        return [l for l in wordMap.values()]


import collections

# 官方解法: sorted() 直接作用于 字符串，返回排好序的字符串列表，字符串没有 sort()方法
class Solution:
    def groupAnagrams(self, strs: list) -> list:
        mp = collections.defaultdict(list)

        for st in strs:
            key = "".join(sorted(st))
            mp[key].append(st)
        
        return list(mp.values())

