# 单词规律.py: https://leetcode-cn.com/problems/word-pattern/

class Solution:
    def wordPattern(self, pattern: str, s: str) -> bool:
        """
            和205题同构字符串是一样的这题
        """
        psMap = dict()
        spMap = dict()
        sList = s.split(" ")
        print(sList)

        if len(pattern) != len(sList): return False
        for i in range(len(pattern)):
            
            if pattern[i] in psMap and psMap[pattern[i]] != sList[i]:
                return False
            
            if sList[i] in spMap and spMap[sList[i]] != pattern[i]:
                return False

            spMap[sList[i]] = pattern[i]
            psMap[pattern[i]] = sList[i]            
        
        return True



        