# 公平的糖果交换： https://leetcode-cn.com/problems/fair-candy-swap/

# hash 查找，利用集合类型， 暴力破解会超时
class Solution:
    def fairCandySwap(self, aliceSizes: list, bobSizes: list) -> list:
        """
            countA - x + y == avg
            x = countA + y - avg
        """
        countA, CountB = sum(aliceSizes), sum(bobSizes)
        avg = (countA + CountB) // 2
        # 利用集合类型
        setA = set(aliceSizes)
        # 找到合适的值
        for y in bobSizes:
            x = countA + y - avg
            if x in setA:
                return [x, y]