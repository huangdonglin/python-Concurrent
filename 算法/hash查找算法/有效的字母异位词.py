# 有效的字母异位词： https://leetcode-cn.com/problems/valid-anagram/

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        """
            创建字典来做（实际上创建一个就行了，一个加，一个减，最后判断是否是0）
        """
        if len(s) != len(t): return False
        
        sCount = dict()
        tCount = dict()
        for i in range(len(s)):
            sCount[s[i]] =  sCount.get(s[i], 0) + 1

        for i in range(len(t)):
            tCount[t[i]] = tCount.get(t[i], 0) + 1
        
        # 进行挨个元素比较
        for i in sCount:
            if sCount.get(i, 0) == 0 or tCount.get(i, 0) == 0:
                return False
            if sCount[i] != tCount[i]:
                return False
        
        return True

