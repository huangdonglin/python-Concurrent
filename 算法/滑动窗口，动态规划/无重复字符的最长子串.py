# 力扣：https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        """ 滑动窗口解题
        """
        n = len(s)
        non_s = set()
        rk = -1
        m_len = 0

        for i in range(n):
            if i != 0:
                non_s.remove(s[i-1])

            while rk + 1 < n and s[rk + 1] not in non_s:
                non_s.add(s[rk+1])
                rk += 1            
            m_len = max(m_len, rk - i +1)

        return m_len
