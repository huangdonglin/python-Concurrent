def encrypt(str_c):
    result = ''
    for i in str_c:
        if i.islower():
            result += chr((ord(i) - ord('a') + 4)%26 + ord('a'))
        elif i.isupper():
            result += chr((ord(i) - ord('A') + 4)%26 + ord('A'))
        else:
            result += i
    return result

print(encrypt('abcdefgABCDEFG'))